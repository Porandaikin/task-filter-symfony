<?php

namespace App\Controller;
use App\Entity\Task;
use App\Service\REST\BravikApi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends AbstractController
{
    /**
     * @Route("/",
     *     options = { "expose" = true },
     *     name = "index_list",
     * )
     */
    public function list()
    {
        return $this->render('index.html.twig');
    }

    /**
     * @Route("/get_filtered_data",
     *     options = { "expose" = true },
     *     name = "get_filtered_data",
     *     methods = "POST"
     * )
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function getFilteredData(Request $request)
    {
        $subject = null;
        $updatedOn = null;
        $content = json_decode($request->getContent(), true);
        if (array_key_exists('subject', $content)) {
            $subject = $content['subject'];
        }
        if (array_key_exists('updatedOn', $content) && $content['updatedOn'] !== '' && $content['updatedOn'] !== null) {
            $updatedOn = new \DateTime($content['updatedOn']);
        }

        $result =  $this->getDoctrine()
            ->getRepository(Task::class)
            ->findByParams($subject, $updatedOn);

        return $this->json($result);
    }
}