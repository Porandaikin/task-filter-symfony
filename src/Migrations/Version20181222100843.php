<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181222100843 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE priority (
              `id` int not null,
              `name` VARCHAR (100) not null,              
               PRIMARY KEY(`id`)
            )
        ');

        $this->addSql('
            CREATE TABLE task (
                `id` int not null,
                `subject` VARCHAR(300) null,
                `description` text null,
                `start_date` DATE,
                `done_ratio` int not null DEFAULT 0,
                `parent` int null,
                `priority_id` int null,
                `created_on` DATETIME,
                `updated_on` DATETIME,
                PRIMARY KEY(`id`),                
                FOREIGN KEY (`priority_id`) REFERENCES  priority(`id`)
            );
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE priority');
        $this->addSql('DROP TABLE task');
    }
}
