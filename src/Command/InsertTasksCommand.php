<?php

namespace App\Command;

use App\Entity\Priority;
use App\Entity\Task;
use App\Service\REST\BravikApi;
use \Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Persistence\ObjectManager;

class InsertTasksCommand extends Command
{
    protected static $defaultName = 'app:insert-tasks';

    /**
     * @var RegistryInterface
     */
    private $doctrine;

    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var OutputInterface
     */
    private $output;

    private $tasks = [];

    private $parents = [];

    private $priorities = [];

    public function __construct(RegistryInterface $doctrine, ObjectManager $manager)
    {
        $this->doctrine = $doctrine;
        $this->manager = $manager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Загрузить задачи')
            ->setHelp('Загрузить новые задачи с сайта');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->initializeData($input, $output);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->insertPriorities();
        $this->insertTasks();
    }

    /**
     * @param string $key
     * @param array  $array
     * @param null   $defaultValue
     *
     * @return mixed|null
     */
    private function getValueByKey(string $key, array $array, $defaultValue = null)
    {
        if (array_key_exists($key, $array)) {
            return $array[$key];
        }

        return $defaultValue;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function initializeData(InputInterface $input, OutputInterface $output)
    {
        $client = new BravikApi();
        $taskData = $client->getTaskData();
        $issues = $this->getValueByKey('issues', $taskData);
        if ($issues !== null) {
            $this->priorities = [];
            $output->writeln('Получено ' . count($issues) . ' новых значений');
            foreach ($issues as $issue) {
                $parent = $this->getValueByKey('parent', $issue);
                $parentId = null;
                if ($parent !== null) {
                    $parentId = (int) $parent['id'];
                }

                $priority = $this->getValueByKey('priority', $issue);
                if ($priority !== null) {
                    $priorityId = (int)$this->getValueByKey('id', $priority);
                    $this->priorities[$priorityId] = [
                        'id' => $priorityId,
                        'name' => $this->getValueByKey('name', $priority),
                    ];
                } else {
                    $priorityId = null;
                }

                $this->tasks[] = [
                    'id' => (int)$this->getValueByKey('id', $issue),
                    'subject' => $this->getValueByKey('subject', $issue),
                    'description' => $this->getValueByKey('description', $issue),
                    'start_date' => new \DateTime($this->getValueByKey('start_date', $issue)),
                    'done_ratio' => $this->getValueByKey('done_ratio', $issue),
                    'parent'    =>  $parentId,
                    'priority_id'   =>  $priorityId,
                    'created_on' => new \DateTime($this->getValueByKey('created_on', $issue)),
                    'updated_on' => new \DateTime($this->getValueByKey('updated_on', $issue)),
                ];

                $parent = $this->getValueByKey('parent', $issue);
                if ($parent !== null) {
                    $this->parents[]= [
                        'task_id' => (int)$this->getValueByKey('id', $issue),
                        'parent_id' =>  (int) $parent['id']
                    ];
                }
            }
        }
    }

    private function  insertPriorities()
    {
        foreach ($this->priorities as $priorityData) {
            $priority = $this->doctrine
                ->getRepository(Priority::class)
                ->find($priorityData['id']);

            if ($priority === null) {
                $priority = new Priority();
                $priority->setId($priorityData['id']);
            }
            $priority->setName($priorityData['name']);
            $this->manager->persist($priority);
        }
        $this->manager->flush();

        $this->output->writeln('Обновлено ' . count($this->priorities) . ' priorities');
    }

    private function insertTasks()
    {
        foreach ($this->tasks as $taskData) {
            $task = $this->doctrine
                ->getRepository(Task::class)
                ->find($taskData['id']);
            if ($task === null) {
                $task = new Task();
                $task->setId($taskData['id']);
            }

            $task->setSubject($taskData['subject']);
            $task->setDescription($taskData['description']);
            $task->setStartDate($taskData['start_date']);
            $task->setDoneRatio($taskData['done_ratio']);
            $task->setParent($taskData['parent']);
            $task->setCreatedOn($taskData['created_on']);
            $task->setUpdatedOn($taskData['created_on']);

            $priorityId = $this->getValueByKey('priority_id', $taskData);
            if ($priorityId !== null) {
                $priority = $this->doctrine
                    ->getRepository(Priority::class)
                    ->find($taskData['priority_id']);
                if ($priority !== null) {
                    $task->setPriority($priority);
                }
            }
            $this->manager->persist($task);
        }
        $this->output->writeln('Обновлено ' . count($this->tasks) . ' tasks');
        $this->manager->flush();
    }
}
