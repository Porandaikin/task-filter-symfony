<?php

namespace App\Service\REST;


use \App\Service\Exceptions\ApiException;

class BravikApi
{
    /**
     * @param string $method
     * @param string $url
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function send(string $method, string $url): array
    {
        try {
            $client = new \GuzzleHttp\Client();
            $resultRequest = $client->request($method, $url);
            if ($resultRequest->getStatusCode() === \App\Service\Config\HttpStatus::OK) {
                $returnData = json_decode($resultRequest->getBody()->getContents(), true);
            } else {
                throw new ApiException(`Status: {$resultRequest->getStatusCode()}; Header: {$resultRequest->getHeader()}; Body: {$resultRequest->getBody()}`);
            }
        } catch (ApiException $e) {
            $this->logRequests($e->getMessage());
            $returnData = [];
        }

        return $returnData;
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTaskData(): array
    {
        return $this->send('GET', 'http://bravik.ru/dev/projects');
    }

    /**
     * @param string $message
     */
    public function logRequests(string $message)
    {

    }
}