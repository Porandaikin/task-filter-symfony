<?php

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Task::class);
    }

    public function findByParams($subject = null, \DateTime $updatedOn = null)
    {
        $queryBuild =  $this->createQueryBuilder('task');
        if ($subject !== null && $subject !== '') {
            $queryBuild->andWhere('task.subject LIKE :subject')
                ->setParameter('subject', "%{$subject}%");
        }
        if ($updatedOn !== null) {
            $queryBuild->andWhere('task.updatedOn = :updatedOn')
                ->setParameter('updatedOn', $updatedOn->format('Y-m-d H:i:s'));
        }
        $queryBuild->orderBy('task.id', 'ASC')
            ->getQuery()
            ->getResult();

        return $queryBuild->getQuery()
            ->getResult();
    }
}
