const routes = require('../../../public/js/fos_js_routes.json');
import Routing from '../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
import $ from 'jquery';
import axios from 'axios';
Routing.setRoutingData(routes);

class Task {
    constructor(){
        this._getFilterResult();
        $('#filter-out-task').click(()=>{
            this._filter();
        })
    }
    _filter() {
        let subject = $('#subject-filter-task').val();
        let updatedOn = $('#updated-on-filter-task').val();
        this._getFilterResult(subject, updatedOn);
    }
    _getFilterResult(subject = null, updatedOn = null) {
        return axios.post(Routing.generate('get_filtered_data'), {
            subject: subject,
            updatedOn: updatedOn

        })
            .then(response => {
                this._setResult(response.data);
            })
            .catch((error) => {
                this._messageLog(error.response.data);
            });
    }
    _setResult(data) {
        let html = '';
        data.forEach((issue) => {
            html += this._itemIssueTemplate(issue);
        });
        $('#result-filtered').html(html);
    }

    _messageLog(message) {
        console.log(message);
    }
    _itemIssueTemplate(issue) {
        return `<tr>
            <th scope="row">${issue.id}</th>
            <td>${issue.subject}</td>
            <td>${issue.description}</td>
            <td>${issue.createdOn}</td>
            <td>${issue.updatedOn}</td>
            <td>${issue.parent}</td>
            <td>${issue.doneRatio}</td>
            <td>${issue.startDate}</td>
            <td>${issue.priority.id} - ${issue.priority.name}</td>
        </tr>`;
    }
}

export default Task;